import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
  render() {
    return (
        <>
      <h1>Welcome to REACT</h1>
      <div>At its core, webpack is a static module bundler for modern JavaScript applications. When webpack processes your application, it internally builds a dependency graph from one or more entry points and then combines every module your project needs into one or more bundles, which are static assets to serve your content from.</div>
      </>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
